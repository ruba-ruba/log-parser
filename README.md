# LogParser

Implements parsing of log files which contain visited page path and visitor ip address

## Requirements

> ruby 2.6.x

## Installation

> 1. Install dependencies

    $ bin/setup

> 2. Install Gem locally:

    $ gem build log_parser.gemspec
    $ gem install log_parser-0.1.0.gem


## Usage

> If gem was installed locally then binary is already setup and ready to use

    $ log_parser parse {file_name}

<sup>example: log_parser parse examples/webserver.log</sup>

> it is also possible to just execute binary

    $ bin/log_parser parse {file_name}

<sup>example: bin/log_parser parse examples/webserver.log</sup>

> In case provided file exist and contains valid data user will be able to output results by entering commands

##### supported commans `views`, `uniq views`, `summary` and `exit`

- `views` - prints page name and views count sorted in descending order
- `uniq views` - prints page name and uniq views count sorted in descending order
- `summary` - outputs both `views` and `uniq_views` sequentially
- `exit` - quits from CLI


## Tech Considerations

#### CLI

> To implement CLI it was easy to choose [Thor](https://github.com/erikhuda/thor).
Thor provides amazing capabilities to write CL Interfaces.

> That decision also was driven by the fact that it felt incomplete to me just to write a program which parses file and outputs `some` results into console.

> In fact, now it is very easy to extend `LogParser` with new functionality. Just implement it and add new command to CLI.

#### development dependencies

>  I use rubocop, simplecov, rspec daily and consider these as Must Have tools.

#### Why Gem?

> well grounded rubyist should be able to wrap their code into ruby Gems.

> It makes much easier in the future to make use different Gem versions and share it as a package.

#### OOD / OOP

> I find that application is well split. Every class responsible to single operation which it does best.

- `CLI` implements interaction with user
- `Parser` just parses every line of log
- `Results` is responsible to add line to overall stored results.
- `Formatter` does nothing but decides how to output.

> Each of classes above could be swapped with another implementation which implements same interfaces

> also it is neat to use separate object for every row (LogParser::Parser::ROW) instead of just operating with strings.
> We could add custom functionality (eg. row validation) and we already have place where to add it.
> That woudn't be that easy if we were operating with strings

#### Performance

> It might look a bit odd that i'm using two different data-stores for views and uniq_views but i'm sure that on the large file that would benefit the performance.


#### To Improve

> CLI class despite providing neat interface for users is hardly testable

> We could also add validation for every parsed line (ie. to check if IP address is valid). However then we need to have a definition of `valid line`.

> Implement counter of parsing errors.