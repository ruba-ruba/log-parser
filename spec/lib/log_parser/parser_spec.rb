require 'spec_helper'

RSpec.describe LogParser::Parser do
  describe '#parse' do
    subject(:parse) do
      described_class.new(results).parse(file.path)
    end

    let(:file) do
      Tempfile.new('test_log').tap do |file|
        file.write(content)
        file.close
      end
    end
    let(:results) { LogParser::Results.new }
    let(:content) {}

    after { file.unlink }

    shared_examples 'file parsing' do
      let(:line1) { ::LogParser::Parser::ROW.new('/help_page/1', '126.318.035.038') }
      let(:line2) { ::LogParser::Parser::ROW.new('/contact', '184.123.665.067') }

      before { allow(results).to receive(:add_view) }

      it 'sends every parsed line obj to Result' do
        parse
        expect(results).to have_received(:add_view).with(line1)
        expect(results).to have_received(:add_view).with(line2)
      end
    end

    context 'with valid data' do
      let(:content) do
        <<~FILEDATA
          /help_page/1 126.318.035.038
          /contact 184.123.665.067
        FILEDATA
      end

      it_behaves_like 'file parsing'
    end

    context 'when file contain empty line' do
      let(:content) do
        <<~FILEDATA
          /help_page/1 126.318.035.038


          /contact 184.123.665.067
        FILEDATA
      end

      it_behaves_like 'file parsing'
    end
  end
end
