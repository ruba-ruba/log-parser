require 'spec_helper'

RSpec.describe LogParser::Results do
  describe '#add_view' do
    subject(:add_views) do
      [line1, line2, line3].each do |row|
        results.add_view(row)
      end
    end

    let(:results) { described_class.new }

    let(:line1) { ::LogParser::Parser::ROW.new('/help_page/1', '126.318.035.038') }
    let(:line2) { ::LogParser::Parser::ROW.new('/help_page/1', '126.318.035.038') }
    let(:line3) { ::LogParser::Parser::ROW.new('/contact', '184.123.665.067') }

    context 'with increments views' do
      let(:expected) do
        [
          ['/help_page/1', 2],
          ['/contact', 1]
        ]
      end

      specify do
        add_views
        expect(results.views.sort).to eq expected.sort
      end
    end

    context 'with increments uniq_views' do
      let(:expected) do
        {
          '/help_page/1' => Set.new(['126.318.035.038']),
          '/contact' => Set.new(['184.123.665.067'])
        }
      end

      specify do
        add_views
        expect(results.uniq_views).to eq expected
      end
    end
  end
end
