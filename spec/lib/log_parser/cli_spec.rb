require 'spec_helper'

RSpec.describe LogParser::CLI do
  describe '#parse' do
    subject(:parse) { instance.parse(file_path) }

    let(:instance) { described_class.new }
    let(:parser) { instance_double(LogParser::Parser) }

    before do
      allow(LogParser::Parser).to receive(:new) { parser }
      allow(parser).to receive(:parse)
      allow(instance).to receive(:initialize_user_interaction)
    end

    context 'with valid file' do
      let(:file_path) { './spec/fixtures/webserver_log_short.log' }

      specify do
        parse
        expect(parser).to have_received(:parse)
      end
    end

    context 'when file does not exist' do
      let(:file_path) { './log.txt' }

      specify do
        expect { parse }.to raise_error SystemExit
      end
    end
  end
end
