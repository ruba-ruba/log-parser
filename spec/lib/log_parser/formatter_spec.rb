require 'spec_helper'

RSpec.describe LogParser::Formatter do
  let(:formatter) do
    described_class.new(parsing_results)
  end
  let(:parsing_results) { instance_double(LogParser::Results) }

  describe '#summary' do
    subject(:summary) { formatter.summary }

    before do
      allow(formatter).to receive(:print_views)
      allow(formatter).to receive(:print_uniq_views)
    end

    it 'prints views & uniq views ordered' do
      summary
      expect(formatter).to have_received(:print_views)
      expect(formatter).to have_received(:print_uniq_views)
    end
  end

  describe '#views' do
    subject(:print_views) { formatter.print_views }

    let(:views) do
      { '/page2' => 2, '/page1' => 1, '/page3' => 3 }
    end
    let(:expected_output) do
      <<~STR
        Total Page Views:
        /page3 - 3 views
        /page2 - 2 views
        /page1 - 1 view
      STR
    end

    before do
      allow(parsing_results).to receive(:views) { views }
    end

    it 'prints page views in DESC order by count' do
      expect { print_views }.to output(expected_output).to_stdout
    end
  end

  describe '#print_uniq_views' do
    subject(:print_uniq_views) { formatter.print_uniq_views }

    let(:uniq_views) do
      {
        '/page2' => ['126.318.035.038', '126.318.035.039'].to_set,
        '/page1' => ['126.318.035.038'].to_set
      }
    end
    let(:expected_output) do
      <<~STR
        Unique Page Views:
        /page2 - 2 views
        /page1 - 1 view
      STR
    end

    before do
      allow(parsing_results).to receive(:uniq_views) { uniq_views }
    end

    it 'prints page views in DESC order by count' do
      expect { print_uniq_views }.to output(expected_output).to_stdout
    end
  end
end
