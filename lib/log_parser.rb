require 'thor'

require 'log_parser/cli'
require 'log_parser/formatter'
require 'log_parser/parser'
require 'log_parser/results'
require 'log_parser/version'

module LogParser
  class Error < StandardError; end
end
