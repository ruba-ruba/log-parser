module LogParser
  class Parser
    ROW = Struct.new(:page, :ip_address)

    def initialize(results)
      @results = results
    end

    def parse(file_path)
      File.foreach(file_path) do |line|
        next if line.strip.empty?

        page, ip_address = line.split
        row = ROW.new(page, ip_address)
        results.add_view(row)
      end
    end

    private

    attr_reader :results
  end
end
