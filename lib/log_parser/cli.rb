module LogParser
  class FileNotExist < StandardError; end

  class CLI < Thor
    desc 'parse --file_path', 'parse log file and print results of WebServer log file'
    def parse(file_path)
      safely_parse_file do
        check_file_exist(file_path)
        parser.parse(file_path)
        puts "successfully parsed '#{file_path}'"
      end

      initialize_user_interaction
    end

    private

    def safely_parse_file
      begin
        yield
      rescue StandardError => err
        warn(err)
        exit 1
      end
    end

    # rubocop:disable Metrics/MethodLength
    def initialize_user_interaction
      puts 'Available commands: `views`, `uniq views`, `summary`. Use `exit` to quit from CLI'
      loop do
        command = ask('type command:')
        case command
        when 'summary'
          formatter.summary
        when 'views'
          formatter.print_views
        when 'uniq views'
          formatter.print_uniq_views
        when 'exit'
          puts 'Bye'
          exit
        else
          puts "unknown command - #{command}"
        end
        puts
      end
    end
    # rubocop:enable Metrics/MethodLength

    def check_file_exist(path)
      return if File.exist?(path) && File.file?(path)

      raise FileNotExist, "#{path} missing"
    end

    def parsing_results
      @parsing_results ||= LogParser::Results.new
    end

    def parser
      @parser ||= ::LogParser::Parser.new(parsing_results)
    end

    def formatter
      Formatter.new(parsing_results)
    end
  end
end
