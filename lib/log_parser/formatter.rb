module LogParser
  class Formatter
    def initialize(results)
      @results = results
    end

    def summary
      print_views
      puts
      print_uniq_views
    end

    def print_views
      puts 'Total Page Views:'
      results.views.sort_by { |_, v| -v }.each do |page, views_count|
        print_view(page, views_count)
      end
    end

    def print_uniq_views
      puts 'Unique Page Views:'
      results.uniq_views.sort_by { |_, v| -v.count }.each do |page, ip_addresses|
        print_view(page, ip_addresses.count)
      end
    end

    private

    attr_reader :results

    def print_view(page, views_count)
      views = pluralize(views_count, 'view')
      puts "#{page} - #{views_count} #{views}"
    end

    def pluralize(count, str)
      count == 1 ? str : "#{str}s"
    end
  end
end
