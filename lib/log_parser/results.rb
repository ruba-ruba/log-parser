module LogParser
  class Results
    attr_reader :views, :uniq_views

    def initialize
      @views = Hash.new { |h, k| h[k] = 0 }
      @uniq_views = Hash.new { |h, k| h[k] = Set.new }
      @raw_data = Hash.new { |h, k| h[k] = [] }
    end

    def add_view(view)
      increment_views_counter(view)
      increment_uniq_views_counter(view)
      store_raw_data(view)
    end

    private

    attr_reader :raw_data

    def increment_views_counter(view)
      views[view.page] += 1
    end

    def increment_uniq_views_counter(view)
      uniq_views[view.page] << view.ip_address
    end

    def store_raw_data(view)
      raw_data[view.page] << view.ip_address
    end
  end
end
