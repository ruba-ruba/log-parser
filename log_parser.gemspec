lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'log_parser/version'

Gem::Specification.new do |spec|
  spec.name          = 'log_parser'
  spec.version       = LogParser::VERSION
  spec.authors       = ['mykhailo Rybak']
  spec.email         = ['mykhailo.rybak.if@gmail.com']

  spec.summary       = 'Web Service Log Parser'
  spec.description   = 'Web Service Log Parser'
  spec.homepage      = 'https://github.com/ruba-ruba'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://github.com/ruba-ruba'

    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://github.com/ruba-ruba'
    spec.metadata['changelog_uri'] = 'https://github.com/ruba-ruba'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'bin'
  spec.executables   = ['log_parser']
  spec.require_paths = ['lib']

  spec.add_dependency 'thor', '~> 1.0.1'

  spec.add_development_dependency 'bundler', '~> 1.17'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.9'
  spec.add_development_dependency 'simplecov', '~> 0.18.5'
end
